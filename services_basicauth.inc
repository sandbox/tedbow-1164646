<?php
// $Id: services_basicauth.inc,v 1.2 2010/12/10 03:10:27 ocyrus Exp $

/**
 * @file
 *  The implementation of the session authentication scheme
 */
define('SERVICES_BASICAUTH_INVALID_MSG','Username or password is not valid.');
function _services_basicauth_authenticate_call($settings, $method, &$args, $auth_args) {
  watchdog('inotary',print_r($args,true));
  watchdog('inotary',print_r($auth_args,true));
  watchdog('inotary',print_r($settings,true));
  watchdog('inotary',"heads=".print_r(apache_request_headers(),true));
  $http_headers = apache_request_headers();
  foreach (array_keys($http_headers) as $key){
    $http_headers[strtolower($key)] = $http_headers[$key];
    unset($http_headers[$key]);
  }
  if(!empty($http_headers['authorization']) ){
    $login_info = _services_basicauth_get_login_info($http_headers['authorization']);
    if($settings['mode']=='both' || $settings['mode']=='drupal_users' ){
      if($uid = user_authenticate($login_info['username'],$login_info['password'])){
        global $user;
        $user = user_load($uid);
      }else if($settings['mode']=='drupal_users'){
        return t(SERVICES_BASICAUTH_INVALID_MSG);
      }
    }else{
      if($settings['static_username'] == $login_info['username'] && $settings['static_password'] == $login_info['password']){
        return;
      }else{
        return t(SERVICES_BASICAUTH_INVALID_MSG);
      }
    }

  }else{
    return t('Username or password were not provided.');
  }
}

function _services_basicauth_security_settings($settings) {
  $form['mode'] = array(
    '#type' => 'select',
    '#default_value' => !empty($settings['mode']) ? $settings['mode'] : '',
    '#title' => t('Mode'),
    '#description' => t('How should the Basic Authorization work?'),
    '#options' => array(
      'drupal_users' => t('Validate against user accounts'),
      'static' => t('Use static username and password below'),
  'both' => t('Check user account first and then check static'),
  ),
  );
  $form['static_username'] = array(
    '#type'          => 'textfield',
    '#default_value' => !empty($settings['static_username']) ? $settings['static_username'] : '',
    '#title'         => t('Username'),
  );
  // @todo Encrypt password on storing
  $form['static_password'] = array(
    '#type'          => 'textfield',
     '#default_value' => !empty($settings['static_password']) ? $settings['static_password'] : '',
    '#title'         => t('Password'),
  );

  return $form;
}
function _services_basicauth_get_login_info($login_string){
  $login_string = str_replace("Basic ","",$login_string);
  $login_string = base64_decode($login_string);
  $colon_index = strpos($login_string,":");
  $login_info['username'] = '';
  $login_info['password'] = '';
  if($colon_index !== FALSE){
    $login_info['username'] = trim(substr($login_string,0,$colon_index));
    $login_info['password'] = trim(substr($login_string,$colon_index + 1));

  }
  watchdog("simpleauth","login-info=".print_r($login_info,true));
  return $login_info;
}
